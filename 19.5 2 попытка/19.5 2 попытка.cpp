﻿// 19.5 2 попытка.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << " voice " << endl;
	}
	virtual ~Animal()
	{}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout <<" The Dog says - Woof " << endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << " The Cat says - Meow " << endl;
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		cout << " The Cow says - Muuu " << endl;
	}
};


int main()
{
	Dog bark;
	bark.Voice();

	Cat soften;
	soften.Voice();

	Cow lowing;
	lowing.Voice();

	cout << "======================================" << endl;

	const int SIZE = 3;
	Animal* arr[SIZE] = { new Dog, new Cat, new Cow };
	for (int i = 0; i < SIZE; i++)
	{
		arr[i]->Voice();
		delete arr[i];
	}
	return 0;
}

